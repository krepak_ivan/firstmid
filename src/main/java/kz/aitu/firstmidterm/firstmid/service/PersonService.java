package kz.aitu.firstmidterm.firstmid.service;

import kz.aitu.firstmidterm.firstmid.model.Person;
import kz.aitu.firstmidterm.firstmid.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonService {
    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll();
    }

    public Person getById(long id){
        return personRepository.findById(id);
    }

    public void deleteById(long id){
        personRepository.deleteById(id);
    }

    public Person createPerson(Person person){
        return personRepository.save(person);
    }

    public Person save(Person person){
        return personRepository.save(person);
    }

    public Person updatePerson(Person person){
        return personRepository.save(person);
    }


}
