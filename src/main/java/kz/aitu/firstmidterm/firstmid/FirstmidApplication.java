package kz.aitu.firstmidterm.firstmid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstmidApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstmidApplication.class, args);
	}

}
