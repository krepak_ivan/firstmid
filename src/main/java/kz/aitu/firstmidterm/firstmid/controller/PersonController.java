package kz.aitu.firstmidterm.firstmid.controller;

import kz.aitu.firstmidterm.firstmid.model.Person;
import kz.aitu.firstmidterm.firstmid.repository.PersonRepository;
import kz.aitu.firstmidterm.firstmid.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users")
    public String showTables(Model model){
        Iterable<Person> in = personRepository.findAll();
        model.addAttribute("in", in);
        return "index";
    }

    @PutMapping("/api/v2/users/")
    public ResponseEntity<?> updatePerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.save(person));
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> createUser(@RequestBody Person person){
        return ResponseEntity.ok(personService.createPerson(person));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deleteUser(@PathVariable long id){
        personService.deleteById(id);
    }
}
