package kz.aitu.firstmidterm.firstmid.repository;

import kz.aitu.firstmidterm.firstmid.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
    Person findById(long id);
}
