insert into person
values
(1, 'Toktar', 'Aubakirov', 'Toretam', '+7 701 689 76 43', 't_aubakirov'),
(2, 'Aidyn', 'Ayimbetov', 'Baikonur', '+7 701 689 76 44', 'a_ayimbetov'),
(3, 'Talgat', 'Musabayev', 'Almaty', '+7 701 689 76 45', 'a_aydyn'),
(4, 'Yuri', 'Lonchakov', 'Astana', '+7 701 689 76 46', 'y_lonchakov'),
(5, 'Ayanbek', 'Serikov', 'Semey', '+7 701 689 76 47', 'a_serikov'),
(6, 'Yuri', 'Gagarin', 'Kiyev', '+7 701 689 76 48', 'y_gagarin'),
(7, 'Aubakirov', 'Toktar', 'Kyzylorda', '+7 701 689 76 49', 'a_toktar'),
(8, 'Ayimbetov', 'Aidyn', 'Alatau', '+7 701 689 76 50', 'a_aidyn'),
(9, 'Musabayev', 'Talgat', 'Shymkent', '+7 701 689 76 51', 'm_talgat'),
(10, 'Gagrin', 'Yuri', 'Vitebsk', '+7 701 689 76 52', 'g_yuri'),
(11, 'German', 'Titov', 'Gomel', '+7 701 689 76 53', 'g_titov');