create table person(
                       id bigint,
                       firstname varchar(128),
                       lastname varchar(128),
                       city varchar(128),
                       phone varchar(128),
                       telegram varchar(128)
);